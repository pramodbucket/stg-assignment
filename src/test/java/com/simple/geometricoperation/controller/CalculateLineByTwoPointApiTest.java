package com.simple.geometricoperation.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;

public class CalculateLineByTwoPointApiTest extends AbstractControllerTest{
    
    /**
     *  Test data of Line.
     * 
     * @return The test data.
     */
    private Line testDataForLine() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(3d);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(7d);
    	coordinate2.setY(1d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line with null as x-axis.
     * 
     * @return The test data.
     */
    private Line testDataForLineWithXaxisNull() {
    	Coordinate coordinate1 = new Coordinate();
    	// Making Null.
    	coordinate1.setX(null);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(7d);
    	coordinate2.setY(1d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
	
    /**
     * Test of calculateLineByTwoPoint method, of class
     * GeometryController with valid data.
     * 
     * @throws Exception
     */
    @Test
    public void testCalculateLineByTwoPoint() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/line")
        		.content(asJSONString(testDataForLine()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("5.0", response,"The response data does not match");
    }
    
    /**
     * Test of calculateLineByTwoPoint method, of class
     * GeometryController with bad request.
     * 
     * @throws Exception
     */
    @Test
    public void testCalculateLineByTwoPointWithMissingXaxis() throws Exception {
    	
    	mockMvc.perform(MockMvcRequestBuilders
        		.post("/line")
        		.content(asJSONString(testDataForLineWithXaxisNull()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }
  
}
