package com.simple.geometricoperation.service.helper;

import org.springframework.stereotype.Component;

import com.simple.geometricoperation.annotation.WebReference;
import com.simple.geometricoperation.vo.Line;

/**
 * Service helper class used to add the reusable implementation code.
 * 
 * @author pramod
 *
 */
@Component
public class GeometryServiceHelper {
	
	/**
	 * Get the slope of the line.
	 * 
	 * @param line The object includes the coordinates.
	 * 
	 * @return The slope of the line.
	 */
	@WebReference("https://www.mathwarehouse.com/algebra/linear_equation/slope-of-a-line.php")
	public Double getSlopeOfLine(Line line) {
		// Slope of the line.
	    Double slopeOfLine = (line.getCoordinate2().getY() - line.getCoordinate1().getY()) 
	    		/ (line.getCoordinate2().getX() - line.getCoordinate1().getX());
		
	    return slopeOfLine;
	}

}
