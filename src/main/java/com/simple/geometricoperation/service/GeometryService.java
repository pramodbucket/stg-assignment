package com.simple.geometricoperation.service;

import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

public interface GeometryService {

	/**
	 * Calculate the line by means of two points.
	 * 
	 * @param line The object includes the coordinates.
	 * 
	 * @return The distance between two points of the plane.
	 */
	Double calculateLineByTwoPoint(Line line);

	/**
	 * Get y-intercept.
	 * 
	 * @param line The object includes the coordinates.
	 * 
	 * @return The y-intercept.
	 */
	Double getYIntercept(Line line);

	/**
	 * Validate parallelism of two lines.
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The status of parallelism of two lines.
	 */
	Boolean validateParallelismOfTwoLines(TwoLine lines);

	/**
	 * Validate the perpendicularity of two lines.
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The status of the perpendicularity of two lines.
	 */
	Boolean validatePerpendicularTwoLines(TwoLine lines);

	/**
	 * Calculate the incidence of two lines.
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The intersection of two lines with coordinates.
	 */
	Coordinate calculateIncidenceOfTwoLines(TwoLine lines);
	
	

}
