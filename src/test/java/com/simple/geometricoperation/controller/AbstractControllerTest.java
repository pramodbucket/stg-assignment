package com.simple.geometricoperation.controller;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.simple.geometricoperation.service.GeometryService;
import com.simple.geometricoperation.service.helper.GeometryServiceHelper;

/**
 * Add the commonly used methods in this class.
 * 
 * @author pramod
 *
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(value = {GeometryController.class, GeometryService.class, GeometryServiceHelper.class})
public abstract class AbstractControllerTest {
	
	
    @Autowired
    protected MockMvc mockMvc;
	    
    /**
     * JSON converter.
     * 
     * @param obj The object to be convert to JSON.
     * @return A JSON String representation of the input object.
     */
    public final String asJSONString(Object obj) {
    	
    	try {
    		ObjectMapper mapper = new ObjectMapper();
    		return mapper.writeValueAsString(obj);    	
    	} catch(JsonProcessingException ex) {
    		throw new RuntimeException("Failed to convert object to JSON format", ex);
    	}
    	
    }

}
