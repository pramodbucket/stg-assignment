package com.simple.geometricoperation.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

public class ValidatePerpendicularTwoLinesApiTest extends AbstractControllerTest{
	

	/**
     *  Test data of Line1.
     * 
     * @return The test data.
     */
    private Line testDataForLine1() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(-2d);
    	coordinate1.setY(6d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(4d);
    	coordinate2.setY(8d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line1 leading to non perpendicular.
     * 
     * @return The test data.
     */
    private Line testDataForLine1NonPerpendicular() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(8d);
    	coordinate1.setY(6d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(4d);
    	coordinate2.setY(3d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line2.
     * 
     * @return The test data.
     */
    private Line testDataForLine2() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(8d);
    	coordinate1.setY(12d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(4d);
    	coordinate2.setY(24d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line with null as x-axis.
     * 
     * @return The test data.
     */
    private Line testDataForLineWithXaxisNull() {
    	Coordinate coordinate1 = new Coordinate();
    	// Making Null.
    	coordinate1.setX(null);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(7d);
    	coordinate2.setY(1d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     * Test data includes two lines.
     * 
     * @return The test data.
     */
    private TwoLine testDataForPerpendicularLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
    
    /**
     * Test data includes two lines.
     * 
     * @return The test data.
     */
    private TwoLine testDataForNonPerpendicularLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1NonPerpendicular());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
    
    /**
     * Test data includes two lines with bad data.
     * 
     * @return The test data.
     */
    private TwoLine testDataForPerpendicularLinesWitBadData() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLineWithXaxisNull());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
	
    /**
     * Test of validatePerpendicularTwoLines method, of class
     * GeometryController with valid data.
     * 
     * @throws Exception
     */
    @Test
    public void testValidatePerpendicularTwoLines() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/perpendicular")
        		.content(asJSONString(testDataForPerpendicularLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("true", response,"The response data does not match");
    }
    
    /**
     * Test of validatePerpendicularTwoLines method, of class
     * GeometryController with non perpendicular lines.
     * 
     * @throws Exception
     */
    @Test
    public void testValidatePerpendicularTwoLinesWithFalse() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/perpendicular")
        		.content(asJSONString(testDataForNonPerpendicularLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("false", response,"The response data does not match");
    }
    
    /**
     * Test of validatePerpendicularTwoLines method, of class
     * GeometryController with bad request.
     * 
     * @throws Exception
     */
    @Test
    public void testValidatePerpendicularTwoLinesWithMissingXaxis() throws Exception {
    	
    	mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/perpendicular")
        		.content(asJSONString(testDataForPerpendicularLinesWitBadData()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }
}
