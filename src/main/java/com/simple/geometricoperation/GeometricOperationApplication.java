package com.simple.geometricoperation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeometricOperationApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeometricOperationApplication.class, args);
	}

}
