package com.simple.geometricoperation;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.simple.geometricoperation.controller.GeometryController;
import com.simple.geometricoperation.service.GeometryService;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class GeometricOperationApplicationTests {

	@Test
	void contextLoads() {
	}

}
