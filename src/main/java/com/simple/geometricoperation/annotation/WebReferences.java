package com.simple.geometricoperation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An annotation to implement the {@link @Repetable} for {@link WebReference} annotation.
 * 
 * @author pramod
 *
 */
@Target(value = {ElementType.TYPE, ElementType.METHOD})
@Retention(value = RetentionPolicy.SOURCE)
public @interface WebReferences {
	
	WebReference[] value();

}
