package com.simple.geometricoperation.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.simple.geometricoperation.annotation.WebReference;
import com.simple.geometricoperation.service.GeometryService;
import com.simple.geometricoperation.service.helper.GeometryServiceHelper;
import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

@Service
public class GeometryServiceImpl implements GeometryService{

	@Autowired
	GeometryServiceHelper geometryServiceHelper;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Double calculateLineByTwoPoint(Line line) {
		
		/* Calculate the distance between two points of the
		 * plane with Cartesian coordinates (x1,y1) and (x2,y2).
		 */
		Double distance = Math.sqrt((line.getCoordinate2().getY() - line.getCoordinate1().getY()) 
				* (line.getCoordinate2().getY() - line.getCoordinate1().getY()) 
				+ (line.getCoordinate2().getX() - line.getCoordinate1().getX()) 
				* (line.getCoordinate2().getX() - line.getCoordinate1().getX()));
		
		return distance;
	}
	

	/**
	 * {@inheritDoc}
	 */
	@WebReference("https://www.mathsisfun.com/y_intercept.html")
	@WebReference("https://www.cuemath.com/geometry/y-intercept/")
	@Override
	public Double getYIntercept(Line line) {

		// Calculate the slope of a line.
		Double slope = geometryServiceHelper.getSlopeOfLine(line);
        // Rule : y = mx + c , identify c.
        Double yIntercept = line.getCoordinate1().getY() - slope * line.getCoordinate1().getX();
        
        return yIntercept;
	}


	/**
	 * {@inheritDoc}
	 */
	@WebReference("https://thefactfactor.com/facts/pure_science/mathematics/coordinate-geometry/condition-for-parallelism-and-perpendicularity/14186/#:~:text=If%20two%20lines%20are%20parallel,m1%20%3D%20m2.")
	@Override
	public Boolean validateParallelismOfTwoLines(TwoLine lines) {
		
		// Calculate the slope of a line.
		Double slope1 = geometryServiceHelper.getSlopeOfLine(lines.getLine1());
		Double slope2 = geometryServiceHelper.getSlopeOfLine(lines.getLine2());
		
		// Parallel : slope1 = slope2
		if(slope1.equals(slope2)){
			return true;
		}else {
			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@WebReference("https://thefactfactor.com/facts/pure_science/mathematics/coordinate-geometry/condition-for-parallelism-and-perpendicularity/14186/#:~:text=If%20two%20lines%20are%20parallel,m1%20%3D%20m2.")
	@Override
	public Boolean validatePerpendicularTwoLines(TwoLine lines) {
		
		// Calculate the slope of a line.
		Double slope1 = geometryServiceHelper.getSlopeOfLine(lines.getLine1());
		Double slope2 = geometryServiceHelper.getSlopeOfLine(lines.getLine2());
		
		Integer perpendicular = (int) (slope1 * slope2);
		
		// Parallel : slope1 * slope2 = -1
		if(perpendicular == -1){
			return true;
		}else {
			return false;
		}
		
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public Coordinate calculateIncidenceOfTwoLines(TwoLine lines) {
		
		// Calculate the slope of a line.
		Double slope1 = geometryServiceHelper.getSlopeOfLine(lines.getLine1());
		Double slope2 = geometryServiceHelper.getSlopeOfLine(lines.getLine2());

		if((slope1 - slope2) == 0) {
			// No Intersection of two lines.
	        return null;		
		} else {
	
			Double yIntercept1 = getYIntercept(lines.getLine1());
			Double yIntercept2 = getYIntercept(lines.getLine2());
			
			Double intersectionX = (yIntercept2 - yIntercept1) / (slope1 - slope2);
			Double intersectionY = slope1 * intersectionX + yIntercept1;

			Coordinate coordinate = new Coordinate();
			coordinate.setX(intersectionX);
			coordinate.setY(intersectionY);
			
			return coordinate;
	    }
		
	}
	
	
	
	
	
	
	
}
