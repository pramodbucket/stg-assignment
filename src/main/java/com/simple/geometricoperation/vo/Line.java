package com.simple.geometricoperation.vo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class Line {
	
	@Valid
	@NotNull
	private Coordinate coordinate1;
	
	@Valid
	@NotNull
	private Coordinate coordinate2;
	
	public Coordinate getCoordinate1() {
		return coordinate1;
	}
	
	public void setCoordinate1(Coordinate coordinate1) {
		this.coordinate1 = coordinate1;
	}
	
	public Coordinate getCoordinate2() {
		return coordinate2;
	}
	
	public void setCoordinate2(Coordinate coordinate2) {
		this.coordinate2 = coordinate2;
	}
	
}
