package com.simple.geometricoperation.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * An Annotation to add reference used to implement the logic.
 * 
 * @author pramod
 */
@Target(value = {ElementType.TYPE, ElementType.METHOD})
@Retention(value = RetentionPolicy.SOURCE)
@Repeatable(WebReferences.class)
public @interface WebReference {
	
	/**
	 * The reference to the web link.
	 * 
	 * @return The reference web link.
	 */
	String value() default "";
	
	/**
	 * Any additional notes on the implementation.
	 * 
	 * @return Any implementation note.
	 */
	String note() default "";

}
