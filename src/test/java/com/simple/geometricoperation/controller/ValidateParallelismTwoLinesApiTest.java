package com.simple.geometricoperation.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

public class ValidateParallelismTwoLinesApiTest extends AbstractControllerTest{
	
	/**
     *  Test data of Line1.
     * 
     * @return The test data.
     */
    private Line testDataForLine1() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(3d);
    	coordinate1.setY(9d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(2d);
    	coordinate2.setY(7d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line1 leading to non parallel.
     * 
     * @return The test data.
     */
    private Line testDataForLine1NonParallel() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(3d);
    	coordinate1.setY(2d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(2d);
    	coordinate2.setY(7d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line2.
     * 
     * @return The test data.
     */
    private Line testDataForLine2() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(-1d);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(0d);
    	coordinate2.setY(6d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line with null as x-axis.
     * 
     * @return The test data.
     */
    private Line testDataForLineWithXaxisNull() {
    	Coordinate coordinate1 = new Coordinate();
    	// Making Null.
    	coordinate1.setX(null);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(7d);
    	coordinate2.setY(1d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     * Test data includes two lines.
     * 
     * @return The test data.
     */
    private TwoLine testDataForParallelLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
    
    /**
     * Test data includes two lines.
     * 
     * @return The test data.
     */
    private TwoLine testDataForNonParallelLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1NonParallel());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
    
    /**
     * Test data includes two lines with bad data.
     * 
     * @return The test data.
     */
    private TwoLine testDataForParallelLinesWitBadData() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLineWithXaxisNull());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
	
    /**
     * Test of validateParallelismTwoLines method, of class
     * GeometryController with valid data.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateParallelismTwoLines() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/parallel")
        		.content(asJSONString(testDataForParallelLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("true", response,"The response data does not match");
    }
    
    /**
     * Test of validateParallelismTwoLines method, of class
     * GeometryController with non parallel lines.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateParallelismTwoLinesWithFalse() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/parallel")
        		.content(asJSONString(testDataForNonParallelLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("false", response,"The response data does not match");
    }
    
    /**
     * Test of validateParallelismTwoLines method, of class
     * GeometryController with bad request.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateParallelismTwoLinesWithMissingXaxis() throws Exception {
    	
    	mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/parallel")
        		.content(asJSONString(testDataForParallelLinesWitBadData()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

}
