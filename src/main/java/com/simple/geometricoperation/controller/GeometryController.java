package com.simple.geometricoperation.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.simple.geometricoperation.service.GeometryService;
import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

@Validated
@RestController
public class GeometryController {
	
	@Autowired
	GeometryService service;
	
	/**
	 * REST End-point to calculate the line by means of two points.
	 * 
	 * @param line The object includes the coordinates.
	 * 
	 * @return The distance between two points of the plane. 
	 */
	@PostMapping("/line")
	public Double  calculateLineByTwoPoint(@Valid @RequestBody Line line) {		
		return service.calculateLineByTwoPoint(line);
	}
	
	/**
	 * REST End-point to get y-intercept.
	 * 
	 * @param line The object includes the coordinates.
	 * 
	 * @return The y-intercept.
	 */
	@PostMapping("/line/yintercept")
	public Double getYIntercept(@Valid @RequestBody Line line) {
		return service.getYIntercept(line);
	}
	
	/**
	 * REST End-point to validate the parallelism of two lines.
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The status of parallelism of two lines.
	 */
	@PostMapping("/twolines/parallel")
	public Boolean validateParallelismTwoLines(@Valid @RequestBody TwoLine lines) {
		return service.validateParallelismOfTwoLines(lines);
	}
	
	/**
	 * REST End-point to validate the perpendicularity of two lines.
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The status of the perpendicularity of two lines.
	 */
	@PostMapping("/twolines/perpendicular")
	public Boolean validatePerpendicularTwoLines(@Valid @RequestBody TwoLine lines) {
		return service.validatePerpendicularTwoLines(lines);
	}
	
	
	/**
	 * REST End-point to calculate the incidence of two lines
	 * 
	 * @param lines The object includes the coordinates.
	 * 
	 * @return The intersection of two lines with coordinates.
	 */
	@PostMapping("/twolines/incidence")
	public String calculateIncidenceOfTwoLines(@Valid @RequestBody TwoLine lines) {
		
		Coordinate coordinate = service.calculateIncidenceOfTwoLines(lines);
		
		if(coordinate == null) {
			return "No Intersection of two lines";			
		}else {
			return "Intersecting Point : "+"x= "+coordinate.getX() + " , " + "y= "+coordinate.getY();
		}
	}
	
	

}
