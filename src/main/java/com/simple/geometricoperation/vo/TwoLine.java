package com.simple.geometricoperation.vo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class TwoLine {
	
	@Valid
	@NotNull
	private Line line1;
	@Valid
	@NotNull
	private Line line2;
	
	public Line getLine1() {
		return line1;
	}
	
	public void setLine1(Line line1) {
		this.line1 = line1;
	}
	
	public Line getLine2() {
		return line2;
	}
	
	public void setLine2(Line line2) {
		this.line2 = line2;
	}
	
}
