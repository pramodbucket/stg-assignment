package com.simple.geometricoperation.vo;

import javax.validation.constraints.NotNull;

/**
 * A Coordinate class defines x-axis and y-axis, 
 * used to locate points on a coordinate plane.
 * 
 * @author pramod
 *
 */
public class Coordinate {
	
	@NotNull
	private Double x;
	@NotNull
	private Double y;
	
	public Double getX() {
		return x;
	}
	
	public void setX(Double x) {
		this.x = x;
	}
	
	public Double getY() {
		return y;
	}
	
	public void setY(Double y) {
		this.y = y;
	}

}
