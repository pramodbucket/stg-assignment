package com.simple.geometricoperation.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.simple.geometricoperation.vo.Coordinate;
import com.simple.geometricoperation.vo.Line;
import com.simple.geometricoperation.vo.TwoLine;

public class CalculateIncidenceOfTwoLinesApiTest extends AbstractControllerTest{
	

	/**
     *  Test data of Line1.
     * 
     * @return The test data.
     */
    private Line testDataForLine1() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(1d);
    	coordinate1.setY(2d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(5d);
    	coordinate2.setY(7d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line1 leading to non incidence.
     * 
     * @return The test data.
     */
    private Line testDataForLine1NonIncidence() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(1d);
    	coordinate1.setY(3d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(5d);
    	coordinate2.setY(7d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line2 leading to non incidence.
     * 
     * @return The test data.
     */
    private Line testDataForLine2NonIncidence() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(4d);
    	coordinate1.setY(6d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(8d);
    	coordinate2.setY(10d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line2.
     * 
     * @return The test data.
     */
    private Line testDataForLine2() {
    	Coordinate coordinate1 = new Coordinate();
    	coordinate1.setX(3d);
    	coordinate1.setY(3d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(4d);
    	coordinate2.setY(5d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     *  Test data of Line with null as x-axis.
     * 
     * @return The test data.
     */
    private Line testDataForLineWithXaxisNull() {
    	Coordinate coordinate1 = new Coordinate();
    	// Making Null.
    	coordinate1.setX(null);
    	coordinate1.setY(4d);
    	
    	Coordinate coordinate2 = new Coordinate();
    	coordinate2.setX(7d);
    	coordinate2.setY(1d);
    	
    	Line line = new Line();
    	line.setCoordinate1(coordinate1);
    	line.setCoordinate2(coordinate2);
    	
    	return line;
    }
    
    /**
     * Test data includes two lines.
     * 
     * @return The test data.
     */
    private TwoLine testDataForIncidenceLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
    
    /**
     * Test data includes two lines for false scenario.
     * 
     * @return The test data.
     */
    private TwoLine testDataForNonIncidenceLines() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLine1NonIncidence());
    	lines.setLine2(testDataForLine2NonIncidence());

    	return lines;
    }
    
    /**
     * Test data includes two lines with bad data.
     * 
     * @return The test data.
     */
    private TwoLine testDataForIncidenceLinesWitBadData() {
    	
    	TwoLine lines = new TwoLine();
    	lines.setLine1(testDataForLineWithXaxisNull());
    	lines.setLine2(testDataForLine2());

    	return lines;
    }
	
    /**
     * Test of validateIncidenceTwoLines method, of class
     * GeometryController with valid data.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateIncidenceTwoLines() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/incidence")
        		.content(asJSONString(testDataForIncidenceLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("Intersecting Point : x= 5.0 , y= 7.0", response,"The response data does not match");
    }
    
    /**
     * Test of validateIncidenceTwoLines method, of class
     * GeometryController with non Incidence lines.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateIncidenceTwoLinesWithFalse() throws Exception {
    	
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/incidence")
        		.content(asJSONString(testDataForNonIncidenceLines()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String response = result.getResponse().getContentAsString();
        
        assertEquals("No Intersection of two lines", response,"The response data does not match");
    }
    
    /**
     * Test of validateIncidenceTwoLines method, of class
     * GeometryController with bad request.
     * 
     * @throws Exception
     */
    @Test
    public void testValidateIncidenceTwoLinesWithMissingXaxis() throws Exception {
    	
    	mockMvc.perform(MockMvcRequestBuilders
        		.post("/twolines/incidence")
        		.content(asJSONString(testDataForIncidenceLinesWitBadData()))
        		.contentType(MediaType.APPLICATION_JSON)
        		.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }
}
